
(function () {


  // Override Marionette.Composite.appendHtml
  // new item should be insert at the right position
  (function () {
    Backbone.Marionette.CompositeView.prototype.appendHtml = function(compositeView, itemView, index){
      if (compositeView.isBuffering) {
        compositeView.elBuffer.appendChild(itemView.el);
      }

      else {
        // If we've already rendered the main collection, just
        // append the new items directly into the element.
        var $container = this.getItemViewContainer(compositeView);

        // specify which place insert new item
        var $indexedElement = $container.children().eq(index);

        if ($indexedElement.length) {
          $indexedElement.before(itemView.el);
        }

        else {
          $container.append(itemView.el);
        }
      }
    }
  })();
})();
